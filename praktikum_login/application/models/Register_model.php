<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register_model extends CI_Model
{

    public function save_register_info($data)
    {
        return $this->db->insert('tb_users', $data);
    }

    public function get_all_level()
    {
        return $this->db->get('tb_level')->result(); // Tampilkan semua data yang ada di tabel level
    }
}
