<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{
    public function getLoginData($data)
    {
        $login['username'] = $data['username'];
        $login['password'] = md5($data['password']);

        $cek = $this->db->get_where('tb_users', $login);
        if ($cek->num_rows() > 0) {
            foreach ($cek->result() as $hasil) {
                $sess_data['logged_in']     = 'yeslogin';
                $sess_data['user_id']       = $hasil->user_id; //mengambil user id
                $sess_data['nama']          = $hasil->nama; // mengambil nama
                $sess_data['username']      = $hasil->username;
                $sess_data['email']         = $hasil->email;
                $sess_data['level_id']      = $hasil->level_id;

                $this->session->set_userdata($sess_data);
                $this->db->where('user_id', $hasil->user_id);
            }
            if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "1") {
                redirect('admin');
            } else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "2") {
                redirect('mahasiswa');
            }
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Maaf, username dan password yang anda masukkan tidak valid.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

            // $this->session->set_flashdata('result_login', "Maaf, username dan password yang anda masukkan tidak valid.");
            redirect('login');
        }
    }
}
