<!DOCTYPE html>
<html>

<head>
    <title>Halaman Admin</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="card">
            <div class="card-header">
                Home Page
            </div>
            <div class="card-body">
                <center>
                    <h1>Selamat Datang Admin<a> Hi, <?php echo $this->session->userdata('nama'); ?></a></h1>
                </center>
                <hr />
                <a href="<?php echo base_url('login/logout'); ?>" class="nav-link">
                    <i class="nav-icon far fa-window-close"></i>
                    <button type="button" class="btn btn-danger">Logout</button>
                </a>
            </div>
        </div>
    </div>
</body>

</html>