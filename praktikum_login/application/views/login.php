<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Login</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
    <section>
        <div class="container h-100">
            <div class="row justify-content-sm-center h-100">
                <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-7 col-sm-9">
                    <div class="text-center my-5">
                        <img src="<?= base_url() ?>assets/image/logo.png" alt="logo" width="100">
                    </div>
                    <div class="card shadow-lg">
                        <div class="card-body p-5">
                            <h2 class="fs-4 card-title fw-bold mb-4">Login</h2>
                            <?php echo form_open('login'); ?>

                            <?php echo $this->session->userdata('message'); ?>
                            <div class="mb-3">
                                <label class="mb-2 text-muted" for="username">Username</label>
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"></input>
                            </div>

                            <div class="mb-3">
                                <label class="mb-2 text-muted" for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"></input>
                            </div>

                            <div class="align-items-center d-flex">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>

                        </div>
                        <div class="card-footer py-3 border-0">
                            <div class="text-center">
                                Belum Punya Akun? <a href="<?php echo base_url('Register') ?>" class="text-dark">Register</a>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-5 text-muted">
                        Copyright &copy; 2022 &mdash; Institut Teknologi Garut
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>