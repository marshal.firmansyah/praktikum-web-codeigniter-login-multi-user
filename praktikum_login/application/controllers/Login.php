<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('Login_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == "") {

			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('login');
			} else {
				$data['username'] 	= $this->input->post('username');
				$data['password'] 	= $this->input->post('password');

				$this->Login_model->getLoginData($data);
			}
		} else if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "1") {
			redirect('admin');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}
