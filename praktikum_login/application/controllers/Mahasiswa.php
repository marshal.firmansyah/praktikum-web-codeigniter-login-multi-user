<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "2") {

            $this->load->view('mahasiswa/home');
        } else {

            redirect('login/logout');
        }
    }
}
