<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('level_id') == "1") {

            $this->load->view('admin/home');
        } else {

            redirect('login/logout');
        }
    }
}
